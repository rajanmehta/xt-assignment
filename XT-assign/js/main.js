
var spaceData = '';

$(document).ready(function () {
    
$('.individual-yearfilter').click(function(){
   $('.individual-yearfilter').removeClass('active');
    $(this).addClass('active');
});    
    
$('.individualspacex-launch').click(function(){
   $('.individualspacex-launch').removeClass('active');
    $(this).addClass('active');
});    
    
$('.individualspacex-landing').click(function(){
   $('.individualspacex-landing').removeClass('active');
    $(this).addClass('active');
});    
    
       $.ajax({  
            type: "GET",  
            url: "https://api.spaceXdata.com/v3/launches?limit=100",  
            data: {},  
            contentType: "application/json; charset=utf-8",  
            dataType: "json",
            success: function (spaceData) {  
              dataFunction(spaceData); 
            },            
            failure: function (jqXHR, textStatus, errorThrown) {                  
                alert("HTTP Status: " + jqXHR.status + "; Error Text: " + jqXHR.responseText); // Display error message  
            }  
        });
    
    
   $('.spacexfilter').on('click',function(){
       
       var url ="https://api.spaceXdata.com/v3/launches?limit=100";
       
       if($('.individualspacex-launch').hasClass('active'))
           {
               url = url + '&launch_success='+ $('.individualspacex-launch.active').text().trim().toLowerCase();
           }
       if($('.individualspacex-landing').hasClass('active'))
           {
               url = url + '&land_success='+ $('.individualspacex-landing.active').text().trim().toLowerCase();
           }
       if($('.individual-yearfilter').hasClass('active'))
           {
               url = url + '&launch_year='+ $('.individual-yearfilter.active').text().trim();
           }
            
      alert(url);
           
       $.ajax({  
            type: "GET",  
            url: url,  
            data: {}, 
            contentType: "application/json; charset=utf-8",  
            dataType: "json",  
            success: function (spaceData) {  
              console.log(spaceData);
                dataFunction(spaceData); 
            },
            failure: function (jqXHR, textStatus, errorThrown) {                  
                alert("HTTP Status: " + jqXHR.status + "; Error Text: " + jqXHR.responseText); // Display error message  
            }  
        }); 
   }); 
    
}); //End of the doc



function dataFunction(spaceData){
    var htmlbind ="";
   //debugger;
    
for(i=0;i<spaceData.length;i++){ 
    var imgsrc='';
    var landSucc = '';
    if(spaceData[i].links != null || spaceData[i].links != undefined){
        if(spaceData[i].links.mission_patch != null || spaceData[i].links.mission_patch != undefined ){
             imgsrc=spaceData[i].links.mission_patch;
        }
        else{
        //debugger;
        imgsrc = 'https://images2.imgbox.com/ab/b8/USCniUHy_o.png';
        }
    }
    else{
        //debugger;
        imgsrc = 'https://images2.imgbox.com/ab/b8/USCniUHy_o.png';
    }
    
    
    if(spaceData[i].rocket.first_stage.cores[0].land_success == null){
    landSucc = 'no value'        
    }
    else{
        landSucc = spaceData[i].rocket.first_stage.cores[0].land_success;
    }
    
    
 htmlbind += ' <div class="individual-spacex" ><div class="spacex-img-div"><img src='+imgsrc+' class="img-responsive" style="width:100%"></div><div class="spacx-contentdiv"><h6 class="missin-name">'+ spaceData[i].mission_name +' # '+ spaceData[i].flight_number +'</h6><div class="spacex-bottom-contnt"><ul class="spacex-listing"><li><span class="spacex-label"><b>Mission Ids:</b></span><span class="spacex-content">'+ spaceData[i].mission_id +'</span></li><li><span class="spacex-label"><b>Launch Year:</b></span><span class="spacex-content" id="launchyear"> '+ spaceData[i].launch_year +' </span></li><li><span class="spacex-label"><b>Successful Launch:</b></span><span class="spacex-content" id="launchsuccess">'+ spaceData[i].launch_success +'</span></li><li><span class="spacex-label"><b>Successful Landing:</b></span><span class="spacex-content" id=""launchlanding>'+landSucc+ '</span></li></ul></div></div></div>';
}
    
if(spaceData.length<=0){
htmlbind += "<h2 class='text-center norecords'>NO Records Found</h2>";
}    
    $('#spacediv').empty();
    $('#spacediv').html(htmlbind);
}
